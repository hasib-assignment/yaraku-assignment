<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('book.index');
})->name('books.view.index');

Route::prefix('books')->name('books.')->group(function () {
    Route::get('fresh', 'BookController@fresh')->name('fresh');
    
    Route::get('{entries?}', 'BookController@index')
        ->where('entries', '[0-9]+')->name('index');

    Route::post('/', 'BookController@store')->name('store');

    Route::patch('{book}', 'BookController@update')
        ->where('book', '[0-9]+')->name('update');

    Route::delete('{book}', 'BookController@destroy')
        ->where('book', '[0-9]+')->name('delete');
        
    Route::get('sort/{column}', 'BookController@sort')
    ->where('column', '[a-z]+')->name('sort');

    Route::get('export/csv/{columns}', 'BookController@exportCsv')
        ->where('columns', '[A-Za-z,]+')->name('export.csv');   
    
    Route::get('export/xml/{columns}', 'BookController@exportXml')
        ->where('columns', '[A-Za-z,]+')->name('export.xml');

    Route::get('search/{query}/{entries?}', 'BookController@search')->name('search');
});
