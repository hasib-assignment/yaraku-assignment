<nav class="navbar sticky-top navbar-expand-md navbar-light navbar-pad bg-white border-btm-e6">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
        <img src="{{ asset('brand/yaraku.png') }}" alt="Brand Image" width="40" height="40" class="d-inline-block align-text-middle">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ms-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <img src="{{asset('imgs/profile.png')}}" class="rounded-circle" height="30" width="30" alt="Profile Picture"> Hasib
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>