<script>
Vue.component('pagination', {
    props: ['links'],
    template: `
        <nav v-if="links" aria-label="Page navigation">
            <ul class="pagination">
                <li class="page-item page-link" v-on:click="fetchData(links.first)">
                    <span aria-hidden="true">&laquo;</span> First
                </li>
                <li v-if="links.prev" class="page-item page-link" v-on:click="fetchData(links.prev)">Previous</li>
                <li v-if="links.next" class="page-item page-link" v-on:click="fetchData(links.next)">Next</li>
                <li class="page-item page-link" v-on:click="fetchData(links.last)">
                    Last <span aria-hidden="true">&raquo;</span>
                </li>
            </ul>
        </nav>
    `,
    methods: {
        fetchData: function(url){
            try{
                this.$emit('paginateData', url)
            } catch (error) {
                console.error(error)
            }
        }
    }
})
</script>