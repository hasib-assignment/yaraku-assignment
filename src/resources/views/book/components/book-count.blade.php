<script>
    Vue.component('book-list-tb-book-count', {
        props: ['meta'],
        template: `
            <span class="position-absolute top-0 start-50 translate-middle badge rounded-pill bg-danger">
                Total @{{meta.total}} Books
                <span class="visually-hidden">Total Books</span>
            </span>
        `,
    })
</script>