<script>
    Vue.component('loading', {
        props: ['loading'],
        template: `
            <div v-if="loading">
                <div class="d-flex align-items-center px-4 py-2 rounded-pill loading-bg-color">
                    <strong>Loading...</strong>
                    <div class="spinner-border spinner-border-sm ms-auto" role="status" aria-hidden="true"></div>
                </div>
            </div>
        `
    })
</script>