<script>
    Vue.component('book-list-tb-search-form', {
        data() {
            return {
                searchQuery: null,
            }
        },
        template: `
            <div class="input-group mb-3">
                <input v-model="searchQuery" @keyup.enter="searchTb()" type="text" class="form-control" placeholder="Search by Title or Author..." aria-label="Search by Title or Author..." aria-describedby="book-search-form-btn">
                <button v-on:click="searchTb()" class="btn btn-outline-secondary" type="button" id="book-search-form-btn">
                    <i class="fs-6 bi-search"></i> Search</button>
            </div>
        `,
        methods: {
            searchTb: function(){
                bus.$emit('searchBookTb', this.searchQuery)
            }
        }
    })
</script>