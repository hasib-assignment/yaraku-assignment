<book-add-form></book-add-form>
<script>
    Vue.component('book-add-form', {
        data() {
            return {
                showSuccessAlert: false,
                isButtonDisabled: false,
                buttonState: 'Add',
                title: '',
                author: '',
            }
        },
        template: `
            <div>
                <div v-if="showSuccessAlert" class="alert alert-primary" role="alert">
                    Book Added
                </div>
                <form ref="bookAddForm" @submit="checkForm" novalidate>
                    <div class="mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control" id="book-add-title-input" placeholder="Book Title" v-model="title" maxlength="500" required>
                            <label for="book-add-title-input">Book Title</label>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                            <div class="invalid-feedback">
                                Please provide book title.
                            </div>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control" id="book-add-author-input" placeholder="Book Author" v-model="author" maxlength="100" required>
                            <label for="book-add-author-input">Book Author</label>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                            <div class="invalid-feedback">
                                Please provide book author.
                            </div>
                        </div>
                    </div>
                    <button type="submit" v-bind:disabled="isButtonDisabled" class="btn btn-outline-primary"><i class="fs-6 bi-patch-plus"></i> @{{this.buttonState}}</button>
                </form>
            </div>
        `,
        methods:{
            checkForm: function (event) {
                var form = this.$refs.bookAddForm
                event.preventDefault()

                if (!form.checkValidity()) {
                    event.stopPropagation()
                } else {
                    this.buttonState = 'Posting...'
                    this.isButtonDisabled = true
                    var url = `${rootUrl}/books`
                    var self = this
                    fetch(url, {
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json",
                            "X-CSRF-TOKEN": document.querySelector('meta[name="csrf-token"]').getAttribute('content')
                        },
                        method: "post",
                        body: JSON.stringify({
                            title: this.title,
                            author: this.author,
                        })
                    }).then(response => response.json())
                    .then(data => {
                        self.buttonState = 'Add'
                        this.isButtonDisabled = false
                        self.showSuccessAlert = true
                        setTimeout(() => {
                            self.showSuccessAlert = false
                        }, 5000)
                        bus.$emit('bookTbRowAdded', data.book)
                    })
                }

                form.classList.add('was-validated')
            }
        }
    })
</script>