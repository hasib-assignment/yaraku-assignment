<script>
    Vue.component('book-edit-form', {
        props: ['id', 'title', 'author'],
        data() {
            return {
                edit_title_input_id: null,
                edit_author_input_id: null,
                book_edit_form: null,
                showSuccessAlert: false,
                isButtonDisabled: false,
                buttonState: 'Edit',
            }
        },
        template: `
            <div>
                <div v-if="showSuccessAlert" class="alert alert-primary" role="alert">
                    Book edited
                </div>
                <form :ref="book_edit_form" @submit="checkForm" novalidate>
                    <div class="mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control" :id="edit_title_input_id" placeholder="Book Title" v-model="title" :value="title" maxlength="500" required>
                            <label for="edit_title_input_id">Book Title</label>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                            <div class="invalid-feedback">
                                Please provide book title.
                            </div>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="form-floating">
                            <input type="text" class="form-control" :id="edit_author_input_id" placeholder="Book Author" v-model="author" :value="author" maxlength="100" required>
                            <label for="edit_author_input_id">Book Author</label>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                            <div class="invalid-feedback">
                                Please provide book author.
                            </div>
                        </div>
                    </div>
                    <button type="submit" v-bind:disabled="isButtonDisabled" class="btn btn-outline-primary"><i class="fs-6 bi-patch-plus"></i> @{{this.buttonState}}</button>
                </form>
            </div>
        `,
        mounted(){
            this.book_edit_form = 'bookEditForm' + this.id
            this.edit_title_input_id = 'edit_title_input_id_' + this.id
            this.edit_author_input_id = 'edit_author_input_id_' + this.id
        },
        methods:{
            checkForm: function (event) {
                var form = this.$refs[this.book_edit_form]
                event.preventDefault()

                if (!form.checkValidity()) {
                    event.stopPropagation()
                } else {
                    this.buttonState = 'Posting...'
                    this.isButtonDisabled = true
                    var url = `${rootUrl}/books/${this.id}`
                    var self = this
                    fetch(url, {
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json",
                            "X-Requested-With": "XMLHttpRequest",
                            "X-CSRF-TOKEN": document.querySelector('meta[name="csrf-token"]').getAttribute('content')
                        },
                        method: "patch",
                        body: JSON.stringify({
                            title: this.title,
                            author: this.author,
                        })
                    }).then(response => response.json())
                    .then(data => {
                        self.buttonState = 'Edit'
                        this.isButtonDisabled = false
                        self.showSuccessAlert = true
                        setTimeout(() => {
                            self.showSuccessAlert = false
                        }, 5000)
                        bus.$emit('bookTbRowEdited', data)
                    })
                }

                form.classList.add('was-validated')
            }
        }
    })
</script>