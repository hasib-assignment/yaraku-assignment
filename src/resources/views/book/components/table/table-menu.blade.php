@include('book.components.table.menu-btns.load-btn')
@include('book.components.table.menu-btns.sort-btn')
@include('book.components.table.menu-btns.entries-btn')
@include('book.components.table.menu-btns.export-btn')
@include('book.components.forms.search-form')
<script>
    Vue.component('book-list-tb-menu', {
        template: `
            <div class="row">
                <div class="col-auto">
                    <div class="btn-group" role="group" aria-label="Table Group Buttons">
                        <book-list-tb-load-btn></book-list-tb-load-btn>
                        <book-list-tb-sort-btn></book-list-tb-sort-btn>
                        <book-list-tb-entries-btn></book-list-tb-entries-btn>
                        <book-list-tb-export-btn></book-list-tb-export-btn>
                    </div>
                </div>
                <div class="col-md-4 ms-auto">
                    <book-list-tb-search-form></book-list-tb-search-form>
                </div>
            </div>
        `,
    })
</script>