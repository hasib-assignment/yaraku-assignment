<script>
    Vue.component('book-list-tb-head', {
        template: `
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Author</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
        `,
    })
</script>