<script>
    Vue.component('book-list-tb-entries-btn', {
        template: `
            <div class="btn-group">
                <button class="btn btn-outline-secondary btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="fs-6 bi-list-ol"></i> Entries
                </button>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="#" type="button" v-on:click="loadEntries(10)">10</a></li>
                    <li><a class="dropdown-item" href="#" type="button" v-on:click="loadEntries(25)">25</a></li>
                    <li><a class="dropdown-item" href="#" type="button" v-on:click="loadEntries(50)">50</a></li>
                    <li><a class="dropdown-item" href="#" type="button" v-on:click="loadEntries(100)">100</a></li>
                </ul>
            </div>
        `,
        methods: {
            loadEntries: function(entries) {
                bus.$emit('loadBookTbWithEntries', entries)
            }
        }
    })
</script>