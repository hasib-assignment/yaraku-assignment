<script>
    Vue.component('book-list-tb-load-btn', {
        template: `
        <button v-on:click="loadBooks()" type="button" class="btn btn-sm btn-outline-primary">
            <i class="fs-6 bi-arrow-counterclockwise"></i> Refresh List</button>
        `,
        methods: {
            loadBooks: function() {
                bus.$emit('loadBookList')
            }
        }
    })
</script>