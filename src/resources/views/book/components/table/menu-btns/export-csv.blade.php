<script>
    Vue.component('export-csv', {
        props: ['columns'],
        data () {
            return {
                downloadUrl: rootUrl + '/books/export/csv/',
                checkedColumns: [],
            }
        },
        template: `
            <div class="offcanvas offcanvas-end px-4 py-4" tabindex="-1" id="csvOffcanvasRight" aria-labelledby="csvOffcanvasRightLabel">

                <div class="offcanvas-header">
                    <h5 id="csvOffcanvasRightLabel">Select Columns to Export</h5>
                    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>

                <div class="offcanvas-body">
                    <export-csv-checkbox
                        v-for="column in columns"
                        v-bind:column="column"
                        v-bind:key="column.name"
                        v-on:inputChange="handleChange"></export-csv-checkbox>

                    <div class="column mx-auto mt-4">
                        <a class="btn btn-outline-primary" :href="downloadUrl + checkedColumns" target="_blank" type="button">
                            <i class="fs-6 bi-download"></i> Download CSV File</a>
                    </div>
                </div>
            </div>
        `,
        methods: {
            handleChange(event) {
                const {value} = event.target
                if(event.target.checked && !this.checkedColumns.includes(value))
                    this.checkedColumns.push(value)
                if(!event.target.checked && this.checkedColumns.includes(value))
                    this.checkedColumns = this.checkedColumns.filter(col => col !== value)
            }
        }
    })

    Vue.component('export-csv-checkbox', {
        props: ['column'],
        data () {
            return {
                checkbox_id: null,
            }
        },
        template: `
            <div class="form-check">
                <input class="form-check-input" v-on:input="(event) => this.$emit('inputChange', event)" type="checkbox" value="" :id="checkbox_id" :value="column.name">
                <label class="form-check-label" :for="checkbox_id">
                    @{{column.name}}
                </label>
            </div>
        `,
        mounted () {
            this.checkbox_id = `${this.column.name}-csv`
        }
    })
</script>