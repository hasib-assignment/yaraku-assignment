<script>
    Vue.component('book-list-tb-sort-btn', {
        template: `
            <div class="btn-group">
                <button class="btn btn-outline-secondary btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="fs-6 bi-sort-up"></i> Sort By
                </button>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="#" type="button" v-on:click="sortBy('title')">Title</a></li>
                    <li><a class="dropdown-item" href="#" type="button" v-on:click="sortBy('author')">Author</a></li>
                </ul>
            </div>
        `,
        methods: {
            sortBy: function(column){
                bus.$emit('sortBookTbBy', column)
            }
        }
    })
</script>