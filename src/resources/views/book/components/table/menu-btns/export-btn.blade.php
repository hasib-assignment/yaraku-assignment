@include('book.components.table.menu-btns.export-csv')
@include('book.components.table.menu-btns.export-xml')
<script>
    Vue.component('book-list-tb-export-btn', {
        template: `
            <div class="btn-group">
                <button class="btn btn-outline-secondary btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="fs-6 bi-download"></i> Export
                </button>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="#" type="button" data-bs-toggle="offcanvas" data-bs-target="#csvOffcanvasRight" aria-controls="offcanvasRight">CSV</a></li>
                    <li><a class="dropdown-item" href="#" type="button" data-bs-toggle="offcanvas" data-bs-target="#xmlOffcanvasRight" aria-controls="offcanvasRight">XML</a></li>
                </ul>
                <export-csv v-bind:columns="[{name: 'Title'},{name: 'Author'}]"></export-csv>
                <export-xml v-bind:columns="[{name: 'Title'},{name: 'Author'}]"></export-xml>
            </div>
        `,
    })
</script>