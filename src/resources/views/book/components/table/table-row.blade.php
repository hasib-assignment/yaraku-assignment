@include('book.components.forms.edit-form')
<script>
    Vue.component('book-tb-row', {
        props: ['book'],
        data() {
            return {
                editOn: false,
                deleteButtonState: 'Delete',
                isDeleteButtonDisabled: false,
                collapse_row_id: null,
                collapse_row_target_id: null,
                collapse_toast_id: null,
            }
        },
        template: `
            <tr>
                <th scope="row">@{{book.id}}</th>
                <td>@{{book.title}}</td>
                <td>@{{book.author}}</td>
                <td>
                    <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                        <button type="button" class="btn btn-sm btn-light" data-bs-toggle="modal" :data-bs-target="collapse_row_target_id"> <i class="fs-6 bi-pen"></i> <span class="ms-1 d-none d-sm-inline">Edit</span></button>
                        
                        <button type="button" v-bind:disabled="isDeleteButtonDisabled" class="btn btn-sm btn-danger" v-on:click="deleteBook(book.id);"> <i class="fs-6 bi-trash2"></i> <span class="ms-1 d-none d-sm-inline">@{{deleteButtonState}}</span></button>
                    </div>
                    <div class="modal fade" :id="collapse_row_id" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <book-edit-form :key="book.id"
                                        v-bind:id="book.id"
                                        v-bind:title="book.title"
                                        v-bind:author="book.author"></book-edit-form>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        `,
        mounted () {
            this.collapse_row_id = `collapseRow-${this.book.id}`
            this.collapse_toast_id = `collapseToast-${this.book.id}`
            this.collapse_row_target_id = `#collapseRow-${this.book.id}`
        },
        methods: {
            deleteBook: function(id) {
                var self = this
                if (confirm(`Delete row with ID: ${id}?`)) {
                    this.deleteButtonState = 'Deleting...'
                    this.isDeleteButtonDisabled = true
                    
                    var url = `${rootUrl}/books/${id}`
                    fetch(url, {
                        headers: {
                            "X-CSRF-TOKEN": document.querySelector('meta[name="csrf-token"]').getAttribute('content')
                        },
                        method: "DELETE",
                    }).then(response => response.status)
                    .then(status => {
                        if(status === 204){
                            bus.$emit('bookTbRowDeleted', id)
                        }
                    })
                }
            }
        }
    })
</script>