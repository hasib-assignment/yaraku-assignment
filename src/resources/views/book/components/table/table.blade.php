@include('book.components.table.table-menu')
@include('book.components.book-count')
@include('book.components.table.table-head')
@include('book.components.table.table-row')

<script>
    Vue.component('book-list-tb', {
        data () {
            return {
                loading: false,
                books: null,
                links: null,
                meta: null,
                error: null,
            }
        },
        template: `
            <div>
                <p class="lead mt-4"><i class="fs-5 bi-list-ol"></i> Book List</p>

                <book-list-tb-menu></book-list-tb-menu>

                <div class="row position-relative">

                    <loading v-bind:loading="this.loading" class="col-3 offset-4"></loading>

                    <div v-if="books">

                        <book-list-tb-book-count
                            v-bind:meta="this.meta"
                            v-bind:key="this.meta.total">
                        </book-list-tb-book-count>

                        <table class="table table-borderless table-responsive">
                            <book-list-tb-head></book-list-tb-head>

                            <tbody>
                                <tr is="book-tb-row"
                                    v-for="book in this.books"
                                    v-bind:key="book.id"
                                    v-bind:book="book"></tr>
                            </tbody>
                        </table>

                        <pagination v-bind:links="this.links" @paginateData="fetchData($event)"></pagination>
                    </div>

                    <div class="py-4" v-else>No available data</div>
                </div>
            </div>
        `,
        methods: {
            fetchData: function(url = `${rootUrl}/books`){
                try{
                    this.books = this.links = this.meta = null
                    this.loading = true
                    var self = this
                    fetch(url)
                    .then(r =>  r.json())
                    .then(data => {
                        self.books = data.data
                        self.links = data.links
                        self.meta  = data.meta
                        self.loading = false
                    })
                } catch (error) {
                    console.error(error)
                }
            },
            searchTb: function(query){
                var url = rootUrl + '/books/search/' + query;
                this.fetchData(url)
            }
        },
        created () {
            // fetch the data when the view is created and the data is
            // already being observed
            this.fetchData()

            var self = this

            bus.$on('bookTbRowAdded', (data) => {
                self.books.push(data);
            })

            bus.$on('loadBookList', () => {
                var url = rootUrl + '/books/fresh';
                self.fetchData(url)
            })

            bus.$on('sortBookTbBy', (column) => {
                var url = rootUrl + '/books/sort/' + column;
                self.fetchData(url)
            })

            bus.$on('loadBookTbWithEntries', (entries) => {
                var url = rootUrl + '/books/' + entries;
                self.fetchData(url)
            })

            bus.$on('searchBookTb', (query) => {
                if(query.length >= 1)
                    self.searchTb(query)
                else
                    self.fetchData()
            })

            bus.$on('bookTbRowEdited', (data) => {
                self.books.some(function(book){
                    if (book.id === data.book.id){
                        self.$set(self.books, self.books.indexOf(book), data.book)
                        return true;
                    }
                });
            })

            bus.$on('bookTbRowDeleted', (id) => {
                self.books = self.books.filter(book => {
                    return book.id !== id
                })
            })
        }
    })
</script>