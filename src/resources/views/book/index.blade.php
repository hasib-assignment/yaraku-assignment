@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row flex-nowrap">
        @include('layouts.components.navbar-left')

        <div class="col">
            <div class="row py-3 ps-4">
                <h1 class="display-6"><i class="fs-2 bi-journals"></i> Books</h1>

                <!-- Button trigger modal -->
                <button type="button" class="btn btn-outline-primary ms-3" style="width: 50%;" data-bs-toggle="modal" data-bs-target="#addBookModal">
                    <i class="fs-6 bi-patch-plus"></i> Add Book
                </button>

                @include('book.components.add-modal')

                <div class="row">@include('book.components.list')</div>
            </div>
            
            <div class="row py-2 ps-4 border-top-e6">
                <span class="ms-1 d-sm-inline"><i class="fs-4 bi-calendar-event"></i> &nbsp; Year: @{{(new Date(Date.now())).getFullYear()}}</span>
            </div>
        </div>
    </div>
</div>
@endsection
