## Requirements
- [Docker](https://docs.docker.com/install)
- [Docker Compose](https://docs.docker.com/compose/install)

## Setup
1. Clone the repository.
2. Start the containers by running `docker-compose up -d` in the project root. Use `sudo` before running `docker` commands for Linux/Mac OS.
3. Install the composer packages by running `docker-compose exec laravel composer install`.
4. Copy '.env' from '.env.example' by running `cd src` from the project root and then run `cp .env.example .env`.
5. Migrate database table with seed by running `docker-compose exec laravel php artisan migrate:fresh --seed`.
6. Access the Laravel instance on `http://localhost` (If there is a "Permission denied" error, run `docker-compose exec laravel chown -R www-data storage`).

Note that the changes you make to local files will be automatically reflected in the container.

## Unit Testing
To run PHPUnit test, run `docker-compose exec laravel ./vendor/bin/phpunit`.

## Assignment Tasks
- Create a list of books, with the following functions, => Done
    - Add a book to the list. => Done
    - Delete a book from the list. => Done
    - Change an authors name => Done
    - Sort by title or author => Done
    - Search for a book by title or author => Done
    - Export the the following in CSV and XML => Done
        - A list with Title and Author => Done
        - A list with only Titles => Done
        - A list with only Authors => Done

## Persistent database
If you want to make sure that the data in the database persists even if the database container is deleted, add a file named `docker-compose.override.yml` in the project root with the following contents.
```
version: "3.7"

services:
  mysql:
    volumes:
    - mysql:/var/lib/mysql

volumes:
  mysql:
```
Then run the following.
```
docker-compose stop \
  && docker-compose rm -f mysql \
  && docker-compose up -d
``` 
