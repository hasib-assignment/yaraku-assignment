<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'author'];

    /**
     * Get book list with conditional clause for session.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetBooks($query) {
        return $query->when(request()->session()->exists('bookTbSortCol'), function ($q) {
            return $q->sort(session('bookTbSortCol'));
        }, function ($q) {
            return $q->orderBy('id');
        })->paginateWithSessionEntries();
    }

    /**
     * Get dynamic pagination with conditional clause for session.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePaginateWithSessionEntries($query) {
        return $query->when(request()->session()->exists('bookTbEntries'), function ($q) {
            return $q->paginate(session('bookTbEntries'));
        }, function ($q) {
            return $q->paginate(10);
        });
    }

    /**
     * Scope a query to sortby a given column.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $column
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSort($query, $column) {
        return $query->orderBy($column);
    }

    /**
     * Scope a query to search for a given search string.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $search
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $search) {
        return $query->where('title', 'LIKE', "%{$search}%")
                ->orWhere('author', 'LIKE', "%{$search}%");
    }
}
