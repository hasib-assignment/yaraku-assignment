<?php

namespace App\Services\Interfaces;

interface XmlServiceInterface {
    public function exportBook(array $cols);
}