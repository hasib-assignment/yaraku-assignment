<?php

namespace App\Services\Interfaces;

interface CsvServiceInterface {
    public function exportBook(array $cols);
}