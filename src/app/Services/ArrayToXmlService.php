<?php

namespace App\Services;

use App\Services\Interfaces\XmlServiceInterface;
use Spatie\ArrayToXml\ArrayToXml;
use App\Models\Book;

class ArrayToXmlService implements XmlServiceInterface {
    /**
     * Book entries for xml.
     *
     * @var array
     */
    private $booksArrForXml = [];

    /**
     * Export book list in xml file.
     *
     * @param array $cols
     * @return Spatie\ArrayToXml\ArrayToXml
     */
    public function exportBook(array $cols) {
        $arrayToXml = new ArrayToXml($this->booksArrForXml, 'books', true, 'UTF-8', '1.1');

        return $arrayToXml->prettify()->toXml();
    }

    /**
     * Create book key for parent tag.
     *
     * @param array $books
     * @return void
     */
    public function setBooksArrForXml(array $books) : void {
        foreach ($books as $key => $book) {
            $this->booksArrForXml['book'.$key] = $book;
        }
    }

    /**
     * Get book array for xml.
     *
     * @return array
     */
    public function getBooksArrForXml() : array {
        return $this->booksArrForXml;
    }

    /**
     * Get array of book list.
     *
     * @param array $cols
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getBooksArr(array $cols) {
        return Book::select($cols)->get()->toArray();
    }
}