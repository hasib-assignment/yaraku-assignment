<?php

namespace App\Services;

use App\Services\Interfaces\CsvServiceInterface;
use App\Exports\BooksExport;
use Maatwebsite\Excel\Excel;

class MaatWebsiteCsvService implements CsvServiceInterface {
    /**
     * Get array of book list.
     *
     * @param array $cols
     * @return Maatwebsite\Excel\Excel::CSV
     */
    public function exportBook(array $cols) {
        return (new BooksExport($cols))->download('books.csv', Excel::CSV, ['Content-Type' => 'text/csv']);
    }
}