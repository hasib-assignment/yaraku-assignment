<?php

namespace App\Http\Traits;

trait SessionTrait {
    /**
     * Set book table `entries` session.
     *
     * @param  string  $entries
     * @return void
     */
    public function setBookTbEntriesSession($entries) : void {
        request()->session()->put('bookTbEntries', $entries);
    }

    /**
     * Set book table `sort column` session.
     *
     * @param  string  $column
     * @return void
     */
    public function setBookTbSortSession($column) : void {
        request()->session()->put('bookTbSortCol', $column);
    }

    /**
     * Forget book table sessions.
     *
     * @return void
     */
    public function forgetBookTbSessions() : void {
        request()->session()->forget(['bookTbEntries', 'bookTbSortCol']);
    }
}
