<?php

namespace App\Http\Traits;

trait HelperTrait {
    /**
     * Create array of columns from string.
     *
     * @param  string  $columns
     * @return Array
     */
    public function getColsArr($columns) : array {
        return explode(",",strtolower($columns));
    }
}
