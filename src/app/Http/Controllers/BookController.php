<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Http\Resources\BookCollection;
use App\Http\Requests\StoreBook;
use App\Http\Requests\UpdateBook;
use Illuminate\Http\Request;
use App\Http\Traits\SessionTrait;
use App\Http\Traits\HelperTrait;
use App\Services\Interfaces\CsvServiceInterface;
use App\Services\Interfaces\XmlServiceInterface;

class BookController extends Controller {

    use SessionTrait, HelperTrait;

    protected $csvService;
    protected $xmlService;

    public function __construct (CsvServiceInterface $csvService, XmlServiceInterface $xmlService) {
        $this->csvService = $csvService;
        $this->xmlService = $xmlService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $entries (optional)
     * @return App\Http\Resources\BookCollection
     */
    public function index(int $entries = 10) : BookCollection {
        $this->setBookTbEntriesSession($entries);

        return new BookCollection(Book::getBooks());
    }

    /**
     * Display a fresh listing of the resource.
     * Delete 'entries', 'sortby' sessions
     * 
     * @return App\Http\Resources\BookCollection
     */
    public function fresh() : BookCollection {
        $this->forgetBookTbSessions();

        return new BookCollection(Book::getBooks());
    }

    /**
     * Display a sorted listing of the resource.
     *
     * @param  string  $column
     * @return App\Http\Resources\BookCollection
     */
    public function sort($column) : BookCollection {
        $this->setBookTbSortSession($column);

        return new BookCollection(Book::sort($column)->paginateWithSessionEntries());
    }

    /**
     * Export book list in csv.
     *
     * @param  string  $columns
     * @return Maatwebsite\Excel\Excel::CSV
     */
    public function exportCsv($columns) {
        $columns = $this->getColsArr($columns);

        return $this->csvService->exportBook($columns);
    }

    /**
     * Export book list in xml.
     *
     * @param  string  $columns
     * @return Spatie\ArrayToXml\ArrayToXml
     */
    public function exportXml($columns) {
        $columns = $this->getColsArr($columns);
        
        $books = $this->xmlService->getBooksArr($columns);
        $this->xmlService->setBooksArrForXml($books);

        $xml = $this->xmlService->exportBook($columns);
        
        return response($xml, 200)
                  ->header('Content-Type', 'application/octet-stream')
                  ->header('Content-Disposition', 'attachment; filename=books.xml')
                  ->header('Pragma', 'no-cache');
    }

    /**
     * Search the resource.
     *
     * @param  string  $query
     * @param  int  $entries (optional)
     * @return App\Http\Resources\BookCollection
     */
    public function search($query, int $entries = 10) : BookCollection {
        return new BookCollection(Book::search($query)->paginate($entries));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBook  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBook $request) {
        $book = Book::create($request->validated());

        return response()->json([
            'book' => $book
        ], 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBook  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBook $request, Book $book) {
        $request = $request->validated();
        $book->update($request);

        return response()->json([
            'book' => $book
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book) {
        $book->delete();

        return response()->json(null, 204);
    }
}
