<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\MaatWebsiteCsvService;
use App\Services\Interfaces\CsvServiceInterface;

class CsvServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CsvServiceInterface::class, MaatWebsiteCsvService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
