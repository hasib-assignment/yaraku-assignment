<?php

namespace App\Exports;

use App\Models\Book;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BooksExport implements FromCollection, WithHeadings
{
    use Exportable;

    protected $cols;

    public function __construct(array $cols)
    {
        $this->cols = $cols;
    }

    public function headings(): array
    {
        return $this->cols;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Book::select($this->cols)->get();
    }
}
