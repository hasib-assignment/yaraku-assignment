<?php

namespace Tests\Feature\Book;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use App\Models\Book;
use App\Http\Resources\BookCollection;

class BookTest extends TestCase
{
    use RefreshDatabase;

    protected $book1;
    protected $book2;

    public function setUp(): void {
        parent::setUp();
        $this->book1 = factory(Book::class)->create([
            'title' => 'The Adventures of Huckleberry Finn',
            'author' => 'Mark Twain',
        ]);
        $this->book2 = factory(Book::class)->create([
            'title' => 'Frankenstein',
            'author' => 'Mary Shelley',
        ]);
        $this->withoutExceptionHandling();
    }

    /**
     * User can create book.
     * 
     * @test
     * @return void
     */
    public function user_can_create_book() {
        $book = factory(Book::class)->make(['title' => 'The Adventures of Huckleberry Finn']);
        $response = $this->postJson(route('books.store'), $book->toArray());

        $response->assertStatus(201);
        
        $response->assertJsonFragment([
            'title'     => $book->title,
            'author'    => $book->author,
        ]);
    }

    /**
     * While updating a book, missing required input field will
     * return an exception.
     * 
     * @test
     * @return void
     */
    public function user_cannot_update_book_with_missing_input_field() {
        $book = factory(Book::class)->create();
        $attributes = [
            'title' => $book->title.'_change',
        ];
        // 'author' field is missing, so expect exception.
        $this->expectException('Illuminate\Validation\ValidationException');
            
        $this->expectExceptionMessage("The given data was invalid");

        $this->patchJson(route('books.update', $book->id), $attributes);

    }

    /**
     * User can update book.
     * 
     * @test
     * @return void
     */
    public function user_can_update_book() {
        $book = factory(Book::class)->create();
        $attributes = [
            'title' => $book->title.'_change',
            'author' => $book->author,
        ];
        $response = $this->patchJson(route('books.update', $book->id), $attributes);

        $response->assertStatus(201);

        $book->refresh();

        $this->assertEquals($attributes['title'], $book->title);
    }

    /**
     * User can get book list.
     * 
     * @test
     * @return void
     */
    public function user_can_get_list_of_book() {
        $books = factory(Book::class, 2)->create();
        $response = $this->json('GET', route('books.index'));

        $response->assertStatus(200);

        $response->assertJsonFragment([
            'title'     => $books[0]->title,
            'author'    => $books[0]->author,
        ]);

        $response->assertJsonFragment([
            'title'     => $books[1]->title,
            'author'    => $books[1]->author,
        ]);
    }

    /**
     * User can delete book.
     * 
     * @test
     * @return void
     */
    public function user_can_delete_book() {
        $book = factory(Book::class)->create();
        $response = $this->deleteJson(route('books.delete', $book->id));

        $response->assertStatus(204);
    }

    /**
     * User can get book list by entry numbers.
     * 
     * @test
     * @return void
     */
    public function user_can_get_list_of_book_by_entries() {
        // Create 15 book entries
        $books = factory(Book::class, 15)->create();
        // Load 10 entries
        $entries = 10;
        $response = $this->json('GET', route('books.index', $entries));

        $response->assertStatus(200);

        $responseData = $response->json();

        $this->assertEquals(count($responseData['data']), $entries);
        // assert if $entries stored in session
        $this->assertTrue(request()->session()->exists('bookTbEntries'));
        $this->assertEquals(request()->session()->get('bookTbEntries'), $entries);
    }

    /**
     * User can refresh book list.
     * 'entries' and 'sortby' sessions will be deleted.
     * 
     * @test
     * @return void
     */
    public function user_can_refresh_list_of_book() {
        // Create 10 book entries
        $books = factory(Book::class, 10)->create();
        $response = $this->json('GET', route('books.fresh'));

        $response->assertStatus(200);

        // assert if stored 'entries' and 'sort column' sessions are deleted
        $this->assertFalse(request()->session()->exists('bookTbEntries'));
        $this->assertFalse(request()->session()->exists('bookTbSortCol'));
    }

    /**
     * User sort book list by book title.
     * 
     * @test
     * @return void
     */
    public function user_can_sort_book_list_by_title() {
        // Sort by 'title'

        $response = $this->json('GET', route('books.sort', 'title'));

        $response->assertStatus(200);

        $responseData = $response->json();

        // First index of 'data' should be of book2
        $this->assertEquals([
            'author' => $this->book2->author,
            'created_at' => $this->book2->created_at,
            'id' => $this->book2->id,
            'title' => $this->book2->title,
            'updated_at' => $this->book2->updated_at,
        ], $responseData['data'][0]);

        // Second index of 'data' should be of book1
        $this->assertEquals([
            'author' => $this->book1->author,
            'created_at' => $this->book1->created_at,
            'id' => $this->book1->id,
            'title' => $this->book1->title,
            'updated_at' => $this->book1->updated_at,
        ], $responseData['data'][1]);

        // assert if 'sort column' stored in session
        $this->assertTrue(request()->session()->exists('bookTbSortCol'));
        $this->assertEquals(request()->session()->get('bookTbSortCol'), 'title');
    }

    /**
     * User sort book list by book author.
     * 
     * @test
     * @return void
     */
    public function user_can_sort_book_list_by_author() {
        // Sort by 'title'

        $response = $this->json('GET', route('books.sort', 'author'));

        $response->assertStatus(200);

        $responseData = $response->json();

        // First index of 'data' should be of book1
        $this->assertEquals([
            'author' => $this->book1->author,
            'created_at' => $this->book1->created_at,
            'id' => $this->book1->id,
            'title' => $this->book1->title,
            'updated_at' => $this->book1->updated_at,
        ], $responseData['data'][0]);

        // Second index of 'data' should be of book2
        $this->assertEquals([
            'author' => $this->book2->author,
            'created_at' => $this->book2->created_at,
            'id' => $this->book2->id,
            'title' => $this->book2->title,
            'updated_at' => $this->book2->updated_at,
        ], $responseData['data'][1]);

        // assert if 'sort column' stored in session
        $this->assertTrue(request()->session()->exists('bookTbSortCol'));
        $this->assertEquals(request()->session()->get('bookTbSortCol'), 'author');
    }

    /**
     * User can search books by author.
     * 
     * @test
     * @return void
     */
    public function user_can_search_books_by_author() {
        // Search by Author
        $response = $this->json('GET', route('books.search', 'Mark'));

        $response->assertStatus(200);

        $responseData = $response->json();

        $this->assertEquals([
            'author' => $this->book1->author,
            'created_at' => $this->book1->created_at,
            'id' => $this->book1->id,
            'title' => $this->book1->title,
            'updated_at' => $this->book1->updated_at,
        ], $responseData['data'][0]);
    }

    /**
     * User can search books by title.
     * 
     * @test
     * @return void
     */
    public function user_can_search_books_by_title() {
        // Search by Title
        $response = $this->json('GET', route('books.search', 'Frankenstein'));

        $response->assertStatus(200);

        $responseData = $response->json();

        $this->assertEquals([
            'author' => $this->book2->author,
            'created_at' => $this->book2->created_at,
            'id' => $this->book2->id,
            'title' => $this->book2->title,
            'updated_at' => $this->book2->updated_at,
        ], $responseData['data'][0]);
    }
}
