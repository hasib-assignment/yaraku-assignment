<?php

namespace Tests\Feature\Book;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel as ExcelFacade;
use Maatwebsite\Excel\Concerns\FromCollection;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use App\Http\Traits\HelperTrait;
use App\Models\Book;

class ExportCsvTest extends TestCase
{
    use RefreshDatabase, HelperTrait;

    protected $csvService;

    public function setUp(): void {
        parent::setUp();

        $this->csvService = $this->app->make('App\Services\Interfaces\CsvServiceInterface');
    }
   
    /**
     * CSV service can export book list array to csv.
     * This test compares a given value with downloaded csv content.
     * 
     * @test
     * @return void
     */
    public function it_can_export_book_list_to_csv() {
        factory(Book::class)->create([
            'title' => 'Book 1', 'author' => 'Author 1',
        ]);
        factory(Book::class)->create([
            'title' => 'Book 2', 'author' => 'Author 2',
        ]);

        ExcelFacade::fake();

        $this->get(route('books.export.csv', ['columns' => 'Title,Author']));

        ExcelFacade::assertDownloaded('books.csv', function(FromCollection  $export) {
            // Assert that the correct export is downloaded.
            $content = $export->collection()->implode('title',',');
            return (strpos($content, 'Book 2') !== false)? true : false;
        });
    }
}
