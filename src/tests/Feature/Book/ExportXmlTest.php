<?php

namespace Tests\Feature\Book;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Support\Collection;
use Spatie\Snapshots\MatchesSnapshots;
use App\Http\Traits\HelperTrait;
use App\Models\Book;

class ExportXmlTest extends TestCase
{
    use RefreshDatabase, MatchesSnapshots, HelperTrait;

    protected $xmlService;

    public function setUp(): void {
        parent::setUp();
        
        $this->xmlService = $this->app->make('App\Services\Interfaces\XmlServiceInterface');
    }

    /**
     * XML service can export book list array to xml.
     * This test matches snapshot of previously stored xml content
     * in '__snapshot__' directory.
     * 
     * @test
     * @return void
     */
    public function it_can_export_book_list_to_xml() {
        $columns = $this->getColsArr('Title,Author');

        $this->assertIsArray($columns);

        $books = $this->getBooksArr();

        $this->xmlService->setBooksArrForXml($books);

        $xml = $this->xmlService->exportBook($columns);

        /*
        * Strip <?xml ....?> tag.
        * Since Spatie\Snapshots\MatchesSnapshots adds <?xml version="1.0"?>,
        * where Spatie\ArrayToXml\ArrayToXml xml service adds <?xml version="1.1" encoding="UTF-8"?>
        */
        $customXML = substr($xml, strpos($xml, '?'.'>') + 2);

        $this->assertMatchesXmlSnapshot($customXML);
    }

    /**
     * @return array
     */
    private function getBooksArr() {
        return [
            ['title' => 'Book 1', 'author' => 'Author 1'],
            ['title' => 'Book 2', 'author' => 'Author 2'],
        ];
    }
}
